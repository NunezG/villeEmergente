﻿using UnityEngine;
using System.Collections;

public class BTNameManager : SingletonScriptableObject<BTNameManager>
{
    public string[] BTNames;
}
