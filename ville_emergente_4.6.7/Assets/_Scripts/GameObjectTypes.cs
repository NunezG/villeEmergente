﻿public enum InteractibleType { Fragment, NPC, SettingPiece };

public enum NPCType { musicien, guide, passant };

public enum FragmentType { liquid, wood, electricity, metal, urban };

public enum CursorType { normal, interactible, fail, fragment };

