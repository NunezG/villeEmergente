/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ANVIL_CONVOLVER_PLAY = 1535194403U;
        static const AkUniqueID ANVIL_CONVOLVER_STOP = 3850059241U;
        static const AkUniqueID ANVIL_PLAY = 767008978U;
        static const AkUniqueID ANVIL_STOP = 2437092940U;
        static const AkUniqueID BREWING_CONVOLVER_PLAY = 3747363609U;
        static const AkUniqueID BREWING_CONVOLVER_STOP = 2043461215U;
        static const AkUniqueID BREWING_PLAY = 610603704U;
        static const AkUniqueID BREWING_STOP = 2983051018U;
        static const AkUniqueID BUILDINGSITE_CONVOLVER_PLAY = 257985868U;
        static const AkUniqueID BUILDINGSITE_CONVOLVER_STOP = 2227770454U;
        static const AkUniqueID BUILDINGSITE_PLAY = 603921033U;
        static const AkUniqueID BUILDINGSITE_STOP = 2126337967U;
        static const AkUniqueID BUSYSTREET_CONVOLVER_PLAY = 289850157U;
        static const AkUniqueID BUSYSTREET_CONVOLVER_STOP = 1407044475U;
        static const AkUniqueID BUSYSTREET_PLAY = 3997590316U;
        static const AkUniqueID BUSYSTREET_STOP = 1672407606U;
        static const AkUniqueID CONVOLUTION_WET_TO0 = 2013603504U;
        static const AkUniqueID CONVOLUTION_WET_TO100 = 837754713U;
        static const AkUniqueID DROPSCARDBOARD_CONVOLVER_PLAY = 3012043839U;
        static const AkUniqueID DROPSCARDBOARD_CONVOLVER_STOP = 2544725597U;
        static const AkUniqueID DROPSCARDBOARD_PLAY = 1374189182U;
        static const AkUniqueID DROPSCARDBOARD_STOP = 2412081984U;
        static const AkUniqueID ELECTRICBUZZ_CONVOLVER_PLAY = 558637385U;
        static const AkUniqueID ELECTRICBUZZ_CONVOLVER_STOP = 2081157487U;
        static const AkUniqueID ELECTRICBUZZ_PLAY = 2776279528U;
        static const AkUniqueID ELECTRICBUZZ_STOP = 817405786U;
        static const AkUniqueID ELECTRICITYARCING_CONVOLVER_PLAY = 3797452220U;
        static const AkUniqueID ELECTRICITYARCING_CONVOLVER_STOP = 2551156774U;
        static const AkUniqueID ELECTRICITYARCING_PLAY = 2107924217U;
        static const AkUniqueID ELECTRICITYARCING_STOP = 404021567U;
        static const AkUniqueID ELECTRICITYSTATIC_CONVOLVER_PLAY = 2137081894U;
        static const AkUniqueID ELECTRICITYSTATIC_CONVOLVER_STOP = 3988186392U;
        static const AkUniqueID ELECTRICITYSTATIC_PLAY = 512634819U;
        static const AkUniqueID ELECTRICITYSTATIC_STOP = 2827602569U;
        static const AkUniqueID FOOTSTEPS_PLAY = 1198733287U;
        static const AkUniqueID FOOTSTEPS_STOP = 4187365685U;
        static const AkUniqueID FRAGMENT_CALL_ELECTRICITY_PLAY = 3885638619U;
        static const AkUniqueID FRAGMENT_CALL_ELECTRICITY_STOP = 2755101329U;
        static const AkUniqueID FRAGMENT_CALL_LIQUID_PLAY = 3141592368U;
        static const AkUniqueID FRAGMENT_CALL_LIQUID_STOP = 1490074322U;
        static const AkUniqueID FRAGMENT_CALL_METAL_PLAY = 933666621U;
        static const AkUniqueID FRAGMENT_CALL_METAL_STOP = 3160982187U;
        static const AkUniqueID FRAGMENT_CALL_PITCH_0 = 3516654290U;
        static const AkUniqueID FRAGMENT_CALL_PITCH_100 = 1252949355U;
        static const AkUniqueID FRAGMENT_CALL_URBAN_PLAY = 2631922986U;
        static const AkUniqueID FRAGMENT_CALL_URBAN_STOP = 4036125012U;
        static const AkUniqueID FRAGMENT_CALL_WOOD_PLAY = 1375190009U;
        static const AkUniqueID FRAGMENT_CALL_WOOD_STOP = 3966254655U;
        static const AkUniqueID GUIDE_IDLE_PLAY = 1128982233U;
        static const AkUniqueID GUIDE_IDLE_STOP = 3761520415U;
        static const AkUniqueID GUIDE_NOUVEAU_SON = 4080525896U;
        static const AkUniqueID GUIDE_ORDRE = 1735048872U;
        static const AkUniqueID GUIDE_SON = 2204075982U;
        static const AkUniqueID HAMMER_CONVOLVER_PLAY = 3334697193U;
        static const AkUniqueID HAMMER_CONVOLVER_STOP = 562249743U;
        static const AkUniqueID HAMMER_PLAY = 1109396808U;
        static const AkUniqueID HAMMER_STOP = 3487067066U;
        static const AkUniqueID INTERACTIONS_SIMPLES_MOTION = 3759079039U;
        static const AkUniqueID LACHER_MORCEAU = 2535466649U;
        static const AkUniqueID LINKER_MORCEAU = 3679869857U;
        static const AkUniqueID LOGCRACK_CONVOLVER_PLAY = 2747240135U;
        static const AkUniqueID LOGCRACK_CONVOLVER_STOP = 1440905237U;
        static const AkUniqueID LOGCRACK_PLAY = 1458620982U;
        static const AkUniqueID LOGCRACK_STOP = 2189158344U;
        static const AkUniqueID METALCREAK_CONVOLVER_PLAY = 3510319782U;
        static const AkUniqueID METALCREAK_CONVOLVER_STOP = 1066456984U;
        static const AkUniqueID METALCREAK_PLAY = 3965185603U;
        static const AkUniqueID METALCREAK_STOP = 1985186057U;
        static const AkUniqueID METALRATTLE_CONVOLVER_PLAY = 3238220620U;
        static const AkUniqueID METALRATTLE_CONVOLVER_STOP = 913037910U;
        static const AkUniqueID METALRATTLE_PLAY = 1751789705U;
        static const AkUniqueID METALRATTLE_STOP = 3274206639U;
        static const AkUniqueID MUSICIEN_DANSE = 59174050U;
        static const AkUniqueID MUSICIEN_IDLE_PLAY = 2327341772U;
        static const AkUniqueID MUSICIEN_IDLE_STOP = 2159062U;
        static const AkUniqueID MUSICIEN_OUVERTURE = 161018858U;
        static const AkUniqueID MUSICIEN_SON = 916635435U;
        static const AkUniqueID MUSICIEN_TOURNE = 2629109720U;
        static const AkUniqueID PADDLE_CONVOLVER_PLAY = 2562287223U;
        static const AkUniqueID PADDLE_CONVOLVER_STOP = 1250729381U;
        static const AkUniqueID PADDLE_PLAY = 2502442470U;
        static const AkUniqueID PADDLE_STOP = 16899800U;
        static const AkUniqueID PASSANT_DANSE = 3122327449U;
        static const AkUniqueID PASSANT_IDLE_PLAY = 3967439363U;
        static const AkUniqueID PASSANT_IDLE_STOP = 2028913609U;
        static const AkUniqueID PASSANT_SON = 1195210688U;
        static const AkUniqueID PASSANT_TOURNE = 4275515877U;
        static const AkUniqueID PRENDRE_MORCEAU = 2138522384U;
        static const AkUniqueID RAIN_2DOBJECT_PLAY = 812837510U;
        static const AkUniqueID RAIN_2DOBJECT_STOP = 2622262392U;
        static const AkUniqueID SFX_RI = 3913070078U;
        static const AkUniqueID SOFTRAIN_CONVOLVER_PLAY = 2812543877U;
        static const AkUniqueID SOFTRAIN_CONVOLVER_STOP = 4195620099U;
        static const AkUniqueID SOFTRAIN_PLAY = 30806100U;
        static const AkUniqueID SOFTRAIN_STOP = 1693234990U;
        static const AkUniqueID SWITCH_ATMO_MOOD = 1340877891U;
        static const AkUniqueID SWITCH_ATMO1 = 2442452516U;
        static const AkUniqueID SWITCH_ATMO2 = 2442452519U;
        static const AkUniqueID SWITCH_ATMO3 = 2442452518U;
        static const AkUniqueID SWITCH_ATMO4 = 2442452513U;
        static const AkUniqueID SWITCH_ATMO5 = 2442452512U;
        static const AkUniqueID SWITCH_ATMO6 = 2442452515U;
        static const AkUniqueID SWITCH_ATMO7 = 2442452514U;
        static const AkUniqueID SWITCH_ATMOMUS = 3926128980U;
        static const AkUniqueID SWITCH_ATMORI1 = 3587869589U;
        static const AkUniqueID SWITCH_ATMORI2 = 3587869590U;
        static const AkUniqueID SWITCH_ATMORI3 = 3587869591U;
        static const AkUniqueID SWITCH_ATMORI4 = 3587869584U;
        static const AkUniqueID SWITCH_ATMORI5 = 3587869585U;
        static const AkUniqueID SWITCH_ATMORI6 = 3587869586U;
        static const AkUniqueID SWITCH_ATMORI7 = 3587869587U;
        static const AkUniqueID SWITCH_DARK_MOOD = 2746150966U;
        static const AkUniqueID SWITCH_DARK1 = 3617898637U;
        static const AkUniqueID SWITCH_DARK2 = 3617898638U;
        static const AkUniqueID SWITCH_DARK3 = 3617898639U;
        static const AkUniqueID SWITCH_DARK4 = 3617898632U;
        static const AkUniqueID SWITCH_DARK5 = 3617898633U;
        static const AkUniqueID SWITCH_DARK6 = 3617898634U;
        static const AkUniqueID SWITCH_DARK7 = 3617898635U;
        static const AkUniqueID SWITCH_DARKMUS = 4044973233U;
        static const AkUniqueID SWITCH_DARKRI1 = 3843097384U;
        static const AkUniqueID SWITCH_DARKRI2 = 3843097387U;
        static const AkUniqueID SWITCH_DARKRI3 = 3843097386U;
        static const AkUniqueID SWITCH_DARKRI4 = 3843097389U;
        static const AkUniqueID SWITCH_DARKRI5 = 3843097388U;
        static const AkUniqueID SWITCH_DARKRI6 = 3843097391U;
        static const AkUniqueID SWITCH_DARKRI7 = 3843097390U;
        static const AkUniqueID SWITCH_DEFAULT = 2406558223U;
        static const AkUniqueID THUNDER_CONVOLVER_PLAY = 3493063959U;
        static const AkUniqueID THUNDER_CONVOLVER_STOP = 2181506373U;
        static const AkUniqueID THUNDER_PLAY = 2706991942U;
        static const AkUniqueID THUNDER_STOP = 221449272U;
        static const AkUniqueID TOUCHER_ELEMENT = 2896867782U;
        static const AkUniqueID TRAIN_CONVOLVER_PLAY = 1092919631U;
        static const AkUniqueID TRAIN_CONVOLVER_STOP = 3810550349U;
        static const AkUniqueID TRAIN_PLAY = 113163278U;
        static const AkUniqueID TRAIN_STOP = 1114702576U;
        static const AkUniqueID VILLE_CALME = 3115394940U;
        static const AkUniqueID WATER_3DOBJECT_PLAY = 193498114U;
        static const AkUniqueID WATER_3DOBJECT_STOP = 836511068U;
        static const AkUniqueID WATERSPLASH_CONVOLVER_PLAY = 763713321U;
        static const AkUniqueID WATERSPLASH_CONVOLVER_STOP = 2286130255U;
        static const AkUniqueID WATERSPLASH_PLAY = 4213022088U;
        static const AkUniqueID WATERSPLASH_STOP = 2295622138U;
        static const AkUniqueID WOODFALL_CONVOLVER_PLAY = 4016804531U;
        static const AkUniqueID WOODFALL_CONVOLVER_STOP = 3105246617U;
        static const AkUniqueID WOODFALL_PLAY = 2459029218U;
        static const AkUniqueID WOODFALL_STOP = 3060465212U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace CONVOLUTIONS_SFX
        {
            static const AkUniqueID GROUP = 980332272U;

            namespace SWITCH
            {
                static const AkUniqueID ATMORI_1 = 4114871465U;
                static const AkUniqueID ATMORI_2 = 4114871466U;
                static const AkUniqueID ATMORI_3 = 4114871467U;
                static const AkUniqueID ATMORI_4 = 4114871468U;
                static const AkUniqueID ATMORI_5 = 4114871469U;
                static const AkUniqueID ATMORI_6 = 4114871470U;
                static const AkUniqueID ATMORI_7 = 4114871471U;
                static const AkUniqueID DARKRI_1 = 1384258054U;
                static const AkUniqueID DARKRI_2 = 1384258053U;
                static const AkUniqueID DARKRI_3 = 1384258052U;
                static const AkUniqueID DARKRI_4 = 1384258051U;
                static const AkUniqueID DARKRI_5 = 1384258050U;
                static const AkUniqueID DARKRI_6 = 1384258049U;
                static const AkUniqueID DARKRI_7 = 1384258048U;
            } // namespace SWITCH
        } // namespace CONVOLUTIONS_SFX

        namespace ELEMENTS_DECOR
        {
            static const AkUniqueID GROUP = 575849320U;

            namespace SWITCH
            {
                static const AkUniqueID ATMO_1 = 1051899170U;
                static const AkUniqueID ATMO_2 = 1051899169U;
                static const AkUniqueID ATMO_3 = 1051899168U;
                static const AkUniqueID ATMO_4 = 1051899175U;
                static const AkUniqueID ATMO_5 = 1051899174U;
                static const AkUniqueID ATMO_6 = 1051899173U;
                static const AkUniqueID ATMO_7 = 1051899172U;
                static const AkUniqueID ATMO_MUS = 3586537614U;
                static const AkUniqueID DARK_1 = 3283321497U;
                static const AkUniqueID DARK_2 = 3283321498U;
                static const AkUniqueID DARK_3 = 3283321499U;
                static const AkUniqueID DARK_4 = 3283321500U;
                static const AkUniqueID DARK_5 = 3283321501U;
                static const AkUniqueID DARK_6 = 3283321502U;
                static const AkUniqueID DARK_7 = 3283321503U;
                static const AkUniqueID DARK_MUS = 1602026157U;
                static const AkUniqueID DEFAULT = 782826392U;
            } // namespace SWITCH
        } // namespace ELEMENTS_DECOR

        namespace PNJ_MOOD
        {
            static const AkUniqueID GROUP = 3937062991U;

            namespace SWITCH
            {
                static const AkUniqueID PNJ_ATMO = 3515469617U;
                static const AkUniqueID PNJ_DARK = 743121882U;
            } // namespace SWITCH
        } // namespace PNJ_MOOD

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID BINAURAL_TO_CONVOLVER = 296257622U;
        static const AkUniqueID CONVOLUTION_WET = 1405780204U;
        static const AkUniqueID FRAGMENT_CALL = 1001606272U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIANCES = 1404066300U;
        static const AkUniqueID BINAURAL = 52442795U;
        static const AkUniqueID CONVOLUTIONS = 579757428U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
